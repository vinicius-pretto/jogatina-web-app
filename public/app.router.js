(function () {
  'use strict';

  angular
    .module('app')
    .config(appRouter);

  function appRouter($stateProvider, $urlRouterProvider) {
    var gamesState = {
      name: 'game',
      url: '/games',
      templateUrl: '/games/game.html',
      controller: 'GameController',
      controllerAs: 'vm'
    };

    var gameFormNew = {
      name: 'game_form_new',
      url: '/games/new',
      templateUrl: '/games/form/game-form.html',
      controller: 'GameFormController',
      controllerAs: 'vm'
    };

    var gameFormEditState = {
      name: 'game_form_edit',
      url: '/games/:id/edit',
      templateUrl: '/games/form/game-form.html',
      controller: 'GameFormController',
      controllerAs: 'vm'
    };

    var customerState = {
      name: 'customer',
      url: '/customers',
      templateUrl: '/customers/customer.html',
      controller: 'CustomerController',
      controllerAs: 'vm'
    }

    var customerFormNewState = {
      name: 'customer_form_new',
      url: '/customers/new',
      templateUrl: '/customers/form/customer-form.html',
      controller: 'CustomerFormController',
      controllerAs: 'vm'
    }

    var customerFormEditState = {
      name: 'customer_form_edit',
      url: '/customers/:id/edit',
      templateUrl: '/customers/form/customer-form.html',
      controller: 'CustomerFormController',
      controllerAs: 'vm'
    }
  
    $stateProvider.state(gamesState);
    $stateProvider.state(gameFormNew);
    $stateProvider.state(gameFormEditState);
    $stateProvider.state(customerState);
    $stateProvider.state(customerFormNewState);
    $stateProvider.state(customerFormEditState);
    $urlRouterProvider.otherwise('/games');
  }
})();
