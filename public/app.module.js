(function () {
  'use strict';

  angular
    .module('app', [
      'ui.router'
    ])
    .constant('API_URL', 'http://localhost:8080/api/v1');
})();
