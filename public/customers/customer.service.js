(function () {
  'use strict';

  angular
    .module('app')
    .service('CustomerService', CustomerService);

    function CustomerService($http) {
      var API_URL = 'http://localhost:8080/api'

      function createCustomer(customer) {
        return $http.post(API_URL + '/customers', customer);
      }

      function findCustomers() {
        return $http.get(API_URL + '/customers')
          .then(function(response) {
            return response.data.customers;
          });
      }

      function findCustomerById(customerId) {
        return $http.get(API_URL + '/customers/' + customerId)
          .then(function(response) {
            return response.data;
          });
      }

      function removeCustomer(customerId) {
        return $http.delete(API_URL + '/customers/' + customerId);
      }

      function updateCustomer(customerId, customer) {
        return $http.put(API_URL + '/customers/' + customerId, customer);
      }
      
      return {
        findCustomers: findCustomers,
        findCustomerById: findCustomerById,
        removeCustomer: removeCustomer,
        createCustomer: createCustomer,
        updateCustomer: updateCustomer
      }
    }
})();
