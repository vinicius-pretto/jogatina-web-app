(function () {
  'use strict';

  angular
    .module('app')
    .controller('CustomerController', CustomerController);

  function CustomerController(CustomerService) {
    var vm = this;
    vm.removeCustomer = removeCustomer;

    activate();

    function activate() {
      findCustomers();
    }

    function findCustomers() {
      CustomerService.findCustomers()
        .then(function(customers) {
          vm.customers = customers;
        });
    }

    function removeCustomer(customerId) {
      CustomerService.removeCustomer(customerId)
        .then(function() {
          alert('Cliente removido!');
          findCustomers();
        });
    }
  }
})();
