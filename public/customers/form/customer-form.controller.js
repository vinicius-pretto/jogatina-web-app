(function () {
  'use strict';

  angular
    .module('app')
    .controller('CustomerFormController', CustomerFormController);

  function CustomerFormController(CustomerService, $state, $stateParams) {
    var vm = this;
    vm.createCustomer = createCustomer;
    vm.saveCustomer = saveCustomer;

    activate();

    function activate() {
      if ($stateParams.id) {
        findCustomerById($stateParams.id);
      }
    }

    function saveCustomer() {
      if ($stateParams.id) {
        updateCustomer($stateParams.id, vm.customer);
      } else {
        createCustomer(vm.customer);
      }
    }

    function findCustomerById(customerId) {
      CustomerService.findCustomerById(customerId)
        .then(function(customer) {
          vm.customer = customer;
        });
    }

    function createCustomer(customer) {
      CustomerService.createCustomer(customer)
        .then(function() {
          $state.go('customer');
        });
    }

    function updateCustomer(customerId, customer) {
      CustomerService.updateCustomer(customerId, customer)
        .then(function() {
          $state.go('customer');
        });
    }
  }
})();
