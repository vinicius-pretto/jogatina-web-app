(function () {
  'use strict';

  angular
    .module('app')
    .controller('GameController', GameController);

  function GameController(GameService, $log, $state) {
    var vm = this;
    vm.removeGame = removeGame;

    activate();

    function activate() {
      var vm = this;
      findGames();
    }

    function findGames() {
      GameService.findGames()
        .then(function(games) {
          vm.games = games;
        })
        .catch(function(error) {
          $log.error('Ocorreu um erro ao buscar os jogos', error);
        });
    }

    function removeGame(gameId) {
      GameService.removeGame(gameId)
        .then(findGames)
        .catch(function(error) {
          $log.error('Ocorreu um erro ao excluir o jogo', error);
        });
    }
  }
})();
