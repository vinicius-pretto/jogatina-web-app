(function () {
  'use strict';

  angular
    .module('app')
    .controller('GameFormController', GameFormController);

    function GameFormController(GameService, $log, $state, $stateParams) {
      var vm = this;
      vm.saveGame = saveGame;

      activate();

      function activate() {
        if ($stateParams.id) {
          findGameById($stateParams.id);
        }
      }

      function saveGame() {
        if ($stateParams.id) {
          updateGame($stateParams.id, vm.game);
        } else {
          createNewGame(vm.game);
        }
      }

      function updateGame(gameId, gameUpdated) {
        GameService.updateGame(gameId, gameUpdated)
          .then(function() {
            $state.go('game');
          })
          .catch(function(error) {
            $log.error('Ocorreu um erro ao atualizar o jogo', error);
          });
      }

      function createNewGame(game) {
        GameService.createGame(game)
          .then(function() {
            $state.go('game');
          })
          .catch(function(error) {
            $log.error('Ocorreu um erro ao cadastrar o jogo', error);
          });
      }

      function findGameById(gameId) {
        GameService.findGameById(gameId)
          .then(function(game) {
            vm.game = game;
          })
          .catch(function(error) {
            $log.error('Ocorreu um erro ao buscar o jogo', error);
          });
      }
    }
})();
