(function () {
  'use strict';

  angular
    .module('app')
    .service('GameService', GameService);

    function GameService($http, API_URL) {
      function createGame(game) {
        return $http.post(API_URL + '/games', game);
      }

      function findGames() {
        return $http.get(API_URL + '/games')
          .then(function(response) {
            return response.data.games;
          });
      }

      function findGameById(gameId) {
        return $http.get(API_URL + '/games/' + gameId)
          .then(function(response) {
            return response.data;
          });
      }

      function updateGame(gameId, gameUpdated) {
        return $http.put(API_URL + '/games/' + gameId, gameUpdated);
      }

      function removeGame(gameId) {
        return $http.delete(API_URL + '/games/' + gameId);
      }

      return {
        createGame: createGame,
        findGames: findGames,
        findGameById: findGameById,
        updateGame: updateGame,
        removeGame: removeGame
      }
    }
})();
