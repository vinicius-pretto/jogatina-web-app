# Jogatina Web App

## Requisitos

* Node.js

## Início

Instalar as dependências do projeto

```
$ npm i
```

Iniciar o servidor

```
$ npm start
```

Após executar os comandos acima, basta acessar http://localhost:3000

## Ferramentas utilizadas neste projeto

* [Angular.js 1.x](https://angularjs.org/)
* [Node.js](https://nodejs.org/en/)
* [Bulma](https://bulma.io/)
